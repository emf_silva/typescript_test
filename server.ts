const pgPromise = require('pg-promise');
const R         = require('ramda');
const request   = require('request-promise');

// Limit the amount of debugging of SQL expressions
const trimLogsSize : number = 200;

var user = "gaearon"; //Default user
var userLocation = "lisbon";

// Database interface
interface DBOptions
  { host      : string
  , database  : string
  , user?     : string
  , password? : string
  , port?     : number
  };

// Actual database options
const options : DBOptions = {
  user: 'postgres',
  password: 'postgres',
  host: 'localhost',
  database: 'lovelystay_test',
};

console.info('Connecting to the database:',
  `${options.user}@${options.host}:${options.port}/${options.database}`);

const pgpDefaultConfig = {
  promiseLib: require('bluebird'),
  // Log all querys
  query(query) {
    console.log('[SQL   ]', R.take(trimLogsSize,query.query));
  },
  // On error, please show me the SQL
  error(err, e) {
    if (e.query) {
      console.error('[SQL   ]', R.take(trimLogsSize,e.query),err);
    }
  }
};

interface GithubUsers
  { id : number
  };

const pgp = pgPromise(pgpDefaultConfig);
const db = pgp(options);

if(process.argv.length == 3){
	if(process.argv[2] == "userLocation"){
		lookForUsers(db);
	}
	if(process.argv[2] == "userPerLocation"){
		howManyUsersPerLocation(db);
	}
	else{
		user = process.argv[2];
		addUser(db);
	}
}

function lookForUsers(db) {
	db.any('SELECT login FROM github_users WHERE location = \''+ userLocation + '\'')
	.then((output) => output.map(x => console.log(x.login)))
	.then(() => process.exit(0));
}

function addUser(db) {
	db.none('CREATE TABLE IF NOT EXISTS github_users (id BIGSERIAL, login TEXT PRIMARY KEY, name TEXT, company TEXT, location TEXT)')
	.then(() => request({
	  uri: 'https://api.github.com/users/'+user,
	  headers: {
	        'User-Agent': 'Request-Promise'
	    },
	  json: true
	  }))
	.then((data: GithubUsers) => db.one(
	  'INSERT INTO github_users (login,name,company,location) VALUES ($[login],$[name],$[company],$[location]) RETURNING id ', data)
	).then(({id}) => console.log(id))
	.then(() => process.exit(0));
}

//How many users per location
function howManyUsersPerLocation(db) {
	db.any('SELECT location FROM github_users')
	.then((output) => output.reduce( (counter, anonymObject) => {
		counter[anonymObject.location] = (counter[anonymObject.location] || 0) + 1;
		return counter;
	}))
	.then((output) => {console.log(output)})
	.then(()=> process.exit(0));
}
